import { SUPPORTED_SERIALIZATION_LANGUAGE_ENUM } from "electron/ipc/supported-serialization-language-enum";

export class CreateBinaryTreeRequest {
    saveFolderPath?: string;
    treeName?: string;
    apiRelativePath?: string;
    apiClassName?: string;
    serializationLanguage?: SUPPORTED_SERIALIZATION_LANGUAGE_ENUM;
}