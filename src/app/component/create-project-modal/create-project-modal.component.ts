import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FolderChosenIpc } from 'electron/ipc/folder-chosen-ipc';
import { GenericRequestwithIdIpc } from 'electron/ipc/generic-request-with-id-ipc';
import { IPC_CHANNEL } from 'electron/ipc/ipc-channel';
import { BinaryTreeService } from 'src/app/service/binary-tree.service';
import { IpcService } from 'src/app/service/ipc.service';
import { SUPPORTED_SERIALIZATION_LANGUAGE, SUPPORTED_SERIALIZATION_LANGUAGE_ENUM } from 'electron/ipc/supported-serialization-language-enum';
import { CreateBinaryTreeRequest } from './create-binary-tree-request';
import { OPEN_TREE_CONF_MODAL_ENUM } from './open-tree-conf-modal-modal-enum';

declare var $: any;

@Component({
  selector: 'app-create-project-modal',
  templateUrl: './create-project-modal.component.html',
  styleUrls: ['./create-project-modal.component.css']
})
export class CreateProjectModalComponent implements OnInit {

  lastChoseFolderRequest?: GenericRequestwithIdIpc;

  currentPath: string = '';

  createBinaryTreeForm: FormGroup;

  createTreeRequest: CreateBinaryTreeRequest = new CreateBinaryTreeRequest();

  OPEN_MODE_ENUM: any = OPEN_TREE_CONF_MODAL_ENUM;

  supportedLanguages: Array<SUPPORTED_SERIALIZATION_LANGUAGE_ENUM> = SUPPORTED_SERIALIZATION_LANGUAGE;

  constructor(private ipc: IpcService, private changeRef: ChangeDetectorRef, public binaryTreeService: BinaryTreeService) {
    this.createBinaryTreeForm = new FormGroup(
      {
        treePath: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(500)]),
        treeName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
        apiRelativePath: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(500)]),
        apiClass: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
      }
    );

    ipc.getipc().on(IPC_CHANNEL.FOLDER_CHOSEN, (event: any, message: FolderChosenIpc) => {
      if(message.id === this.lastChoseFolderRequest?.id) {
        this.currentPath = message.folder;
        this.createBinaryTreeForm.patchValue(
          {treePath: this.currentPath }
        );
        changeRef.detectChanges();
      }
    });

    $('.sticky').sticky();
   }

  ngOnInit(): void {
    $('#create-tree-modal').modal({
      onShow: () => {
        if(this.binaryTreeService.openTreeConfModalMode === OPEN_TREE_CONF_MODAL_ENUM.CREATE_NEW_TREE) {
          this.createBinaryTreeForm.patchValue({treePath: '', treeName: '', apiRelativePath: '',apiClass: '' });
          $('#language-dd').dropdown('set selected', SUPPORTED_SERIALIZATION_LANGUAGE_ENUM.TYPESCRIPT);
        } else if(this.binaryTreeService.currentTree) {
          this.createBinaryTreeForm.patchValue({
            treePath: this.binaryTreeService.currentTree.savePath,
            treeName: this.binaryTreeService.currentTree.treeName,
            apiRelativePath: this.binaryTreeService.currentTree.apiRelativePath,
            apiClass: this.binaryTreeService.currentTree.apiClassName
          });
          $('#language-dd').dropdown('set selected', this.binaryTreeService.currentTree.serializationLanguage);
        }
        this.changeRef.detectChanges();
      }
    });
  }

  chooseCreateFolder(): void {
    this.lastChoseFolderRequest = new GenericRequestwithIdIpc();
    this.ipc.send1(IPC_CHANNEL.CHOOSE_FOLDER, this.lastChoseFolderRequest);
  }

  onCreateBinaryTree(): void {
    if(this.createBinaryTreeForm.valid === true) {
      this.createTreeRequest.saveFolderPath = this.createBinaryTreeForm.get("treePath")?.value;
      this.createTreeRequest.treeName = this.createBinaryTreeForm.get("treeName")?.value;
      this.createTreeRequest.apiRelativePath = this.createBinaryTreeForm.get("apiRelativePath")?.value;
      this.createTreeRequest.apiClassName = this.createBinaryTreeForm.get("apiClass")?.value;
      this.createTreeRequest.serializationLanguage = $('#language-dd').dropdown('get value');
      this.binaryTreeService.createBinaryTree(this.createTreeRequest);
    } else {
      console.log("Invalid")
    }
  }

  onSaveBinaryTreeConf(): void {
    if(this.createBinaryTreeForm.valid === true && this.binaryTreeService.currentTree) {
      this.binaryTreeService.currentTree.savePath = this.createBinaryTreeForm.get("treePath")?.value;
      this.binaryTreeService.currentTree.treeName = this.createBinaryTreeForm.get("treeName")?.value;
      this.binaryTreeService.currentTree.apiRelativePath = this.createBinaryTreeForm.get("apiRelativePath")?.value;
      this.binaryTreeService.currentTree.apiClassName = this.createBinaryTreeForm.get("apiClass")?.value;
      this.binaryTreeService.currentTree.serializationLanguage = $('#language-dd').dropdown('get value');
    } else {
      console.log("Invalid")
    }
  }
}
