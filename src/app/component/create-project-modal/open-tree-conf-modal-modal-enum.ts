export enum OPEN_TREE_CONF_MODAL_ENUM {
    CREATE_NEW_TREE = 'new',
    EDIT_CURRENT_TREE = 'edit'
}