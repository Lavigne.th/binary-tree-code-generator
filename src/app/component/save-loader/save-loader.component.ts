import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { IPC_CHANNEL, SAVE_TREE_STATE } from 'electron/ipc/ipc-channel';
import { IpcService } from 'src/app/service/ipc.service';

@Component({
  selector: 'app-save-loader',
  templateUrl: './save-loader.component.html',
  styleUrls: ['./save-loader.component.css']
})
export class SaveLoaderComponent implements OnInit {

  loading: boolean = false;

  constructor(private ipc: IpcService, changeRef: ChangeDetectorRef) {
    ipc.getipc().on(IPC_CHANNEL.SAVE_TREE_STATE, (event: any, state: SAVE_TREE_STATE) => {
      if(state === SAVE_TREE_STATE.SAVING) {
        this.loading = true;
      } else if(state === SAVE_TREE_STATE.ENDED) {
        this.loading = false;
      }
      changeRef.detectChanges();
    });
   }

  ngOnInit(): void {
  }

}
