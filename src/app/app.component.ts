import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { IPC_CHANNEL } from 'electron/ipc/ipc-channel';
import { OPEN_TREE_CONF_MODAL_ENUM } from './component/create-project-modal/open-tree-conf-modal-modal-enum';
import { BinaryTreeService } from './service/binary-tree.service';
import { CanvasService } from './service/canvas.service';
import { IpcService } from './service/ipc.service';
import { NODE_ACTION } from './tree/node-action';
import { TreeNode } from './tree/tree-node';

declare var $: any;
declare var electron: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'binary-tree-generator';

  private isMovingSideWays: boolean = false;

  private isZooming: boolean = false;

  private currentZoom: number = 1;

  private MAX_ZOOM: number = 3;

  private MIN_ZOOM: number = 0.2;

  private ZOOM_STEP: number = 0.1;

  private xLastMouse: number = -1;

  private yLastMouse: number = -1;

  private MOVE_OFFSET: number = 30;

  currentNodeAction: NODE_ACTION = NODE_ACTION.NONE;

  currentNode?: TreeNode = undefined;

  NODE_ACTION_ENUM: any = NODE_ACTION;

  constructor(private ipc: IpcService, public binaryTreeService: BinaryTreeService, private changeRef: ChangeDetectorRef, private canvasService: CanvasService) {
  }

  ngOnInit(): void {
    this.binaryTreeService.changeZoom.subscribe((newZoom: number) => {
      this.currentZoom = newZoom;
      this.updateZoom();
    });
    this.canvasService.init();
    this.canvasService.resizeCanvasSize();
    window.addEventListener('resize', () => {
      this.canvasService.resizeCanvasSize();
      this.canvasService.render();
    }, true);
  
    this.binaryTreeService.onBinaryTreeCreated.subscribe(() => {
      this.changeRef.detectChanges();
    });
    this.binaryTreeService.onBinaryTreeActionChanged.subscribe(() => {
      this.currentNodeAction = <NODE_ACTION>this.binaryTreeService.currentTree?.currentAction;
      this.currentNode = <TreeNode | undefined>this.binaryTreeService.currentTree?.currentFocusedNode;
      this.changeRef.detectChanges();
    });
    this.binaryTreeService.onBinaryTreeRelationChanged.subscribe(() => {
      this.canvasService.render();
    });

    (<any>document.querySelector('#bottom-menu')).addEventListener('click', (ev: MouseEvent) => {
      ev.stopPropagation();
    }, false);

    document.addEventListener('click', (ev: MouseEvent) => {
      this.binaryTreeService.resetNodeFocus();
    });

    document.addEventListener('keydown', (ev: KeyboardEvent) => {
      if(ev.keyCode === 17) {
        this.isZooming = true;
      } else if(ev.keyCode === 16) {
        this.isMovingSideWays = true;
      }
    });

    document.addEventListener('keyup', (ev: KeyboardEvent) => {
      if(ev.keyCode === 17) {
        this.isZooming = false;
      } else if(ev.keyCode === 16) {
        this.isMovingSideWays = false;
      }
    });
    window.addEventListener("wheel", (ev: WheelEvent) => {
      let changed: boolean = false;
      //ZOOM IN / OUT
      if(this.isZooming === true) {
        if(ev.deltaY > 0 && this.currentZoom - this.ZOOM_STEP >= this.MIN_ZOOM) {
          this.currentZoom -= this.ZOOM_STEP;
          changed = true;
        } else if(ev.deltaY < 0 && this.currentZoom + this.ZOOM_STEP <= this.MAX_ZOOM)  {
          this.currentZoom += this.ZOOM_STEP;
          changed = true;
        }
        if(changed === true) {
          //electron.webFrame.setZoomFactor(this.currentZoom);
          this.updateZoom();
        }

        //Move UP / DOWN / LEFT / RIGHT
      } else if (this.binaryTreeService.currentTree) {
        for (let node of this.binaryTreeService.currentTree.nodes) {
          let multiplier: number = ev.deltaY > 0 ? 1 : -1;
          if (this.isMovingSideWays === true) {
            node.moveBy(this.MOVE_OFFSET * multiplier, 0);
          } else {
            node.moveBy(0, this.MOVE_OFFSET * multiplier);
          }
        }
        this.canvasService.render();
      }
    });

    document.addEventListener('mousemove', (ev: MouseEvent) => {
      if(this.xLastMouse === -1 || this.binaryTreeService.isDragging() === false) {
        this.xLastMouse = ev.clientX;
        this.yLastMouse = ev.clientY;
      } else {
        //Make the move bigger when the scene is zoomed out and vice-versa so that the node follow the mouse
        let zoomEffect: number = (1 / this.currentZoom);
        let xOffset: number = (ev.clientX - this.xLastMouse) * zoomEffect;
        let yOffset: number = (ev.clientY - this.yLastMouse) * zoomEffect;
        if(this.binaryTreeService.isDragging() === true) {
          this.binaryTreeService.currentTree?.currentDraggedNode?.moveBy(xOffset, yOffset);
          this.canvasService.render();
        }

        this.xLastMouse = ev.clientX;
        this.yLastMouse = ev.clientY;
      }
    });

    document.addEventListener('mouseup', (ev: MouseEvent) => {
      this.binaryTreeService.stopDragging();
    });
  }

  private updateZoom(): void {
    if(this.binaryTreeService.currentTree) {
      for(let node of this.binaryTreeService.currentTree.nodes) {
        node.setZoom(this.currentZoom);
      }
    }
    this.changeRef.detectChanges();
    this.canvasService.render();
  }

  onReloadPage(): void {
    this.ipc.send1(IPC_CHANNEL.RELOAD_PAGE, null);
  }

  onOpenCreateTreeModal(): void {
    this.binaryTreeService.openTreeConfModalMode = OPEN_TREE_CONF_MODAL_ENUM.CREATE_NEW_TREE;
    $('#create-tree-modal').modal('show');
  }

  onEditBinaryTreeConf(): void {
    this.binaryTreeService.openTreeConfModalMode = OPEN_TREE_CONF_MODAL_ENUM.EDIT_CURRENT_TREE;
    $('#create-tree-modal').modal('show');
  }

  onAddNode(): void {
    let clone: any = document.querySelector('.clone.node')?.cloneNode(true);
    clone.classList.remove('clone');
    this.binaryTreeService.addNode(clone, this.currentZoom);
    document.querySelector('main')?.appendChild(clone);
  }

  getZoomStr(): string {
    return Math.round(this.currentZoom * 100) + ' %';
  }

  changeNodeName(): void {
    let name: string | undefined = (<any>document.querySelector('#node-name-input')).value;
    if(name !== undefined && name.length > 4) {
      this.binaryTreeService.changeNodeName(this.currentNode, name);
    } else {

    }
  }

  onSaveBinaryTree() {
    this.binaryTreeService.saveTree(this.currentZoom);
  }

  setRootNode(): void {
    this.binaryTreeService.setRootNode();
  }

  onOpenExistingTree(): void {
    this.binaryTreeService.loadTree();
  }
}
