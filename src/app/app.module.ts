import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateProjectModalComponent } from './component/create-project-modal/create-project-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SaveLoaderComponent } from './component/save-loader/save-loader.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateProjectModalComponent,
    SaveLoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
