import { showToast, TOAST_POSITION, TOAST_TYPE } from "../service/toast";
import { BinaryTree } from "./binary-tree";
import { NODE_ACTION } from "./node-action";

export class TreeNode {
    private static CURRENT_INSTANCE_ID = 0;
    
    id: number = -1;
    instanceId: number;
    serializedInstanceId?: number;
    name: string = 'NoName';
    htmlElement: any;
    htmlParentElement: any;
    htmlSuccessElement: any;
    htmlFailureElement: any;

    y: number = 0;
    x: number = 0;    
    isRoot: boolean = false;
    currentZoom: number;

    parentNode?: TreeNode;
    successNode?: TreeNode;
    failureNode?: TreeNode;

    width: number = 300;
    height: number = 150;

    binaryTree: BinaryTree;

    hasBeenCloned: boolean = false;

    private onMove: any;
    private moveHandler: any;

    private onEdit: any;
    private editHandler: any;

    private onRemove: any;
    private removeHandler: any;

    private onSetParent: any;
    private setParentHandler: any;

    private onSetSuccessNode: any;
    private setSuccessNodeHandler: any;

    private onSetFailureNode: any;
    private setFailureNodeHandler: any;

    private onUnsetParent: any;
    private unsetParentHandler: any;

    private onUnsetSuccessNode: any;
    private unsetSuccessNodeHandler: any;

    private onUnsetFailureNode: any;
    private unsetFailureNodeHandler: any;

    private onCloneNode: any;
    private cloneNodeHandler: any;

    constructor(binaryTree: BinaryTree, id: number, isRoot: boolean, name: string, htmlElement: any, xPosition: number, yPosition: number, currentZoom: number, hasBeenCloned: boolean) {
        this.binaryTree = binaryTree;
        this.htmlElement = htmlElement;
        this.instanceId = TreeNode.CURRENT_INSTANCE_ID++;
        this.setId(id);
        this.setName(name);
        this.setCloned(hasBeenCloned);

        this.x = xPosition;
        this.y = yPosition;
        this.currentZoom = currentZoom;
        this.updateScale();
        this.draw();

        this.htmlParentElement = htmlElement.querySelector('.parent-node-icon');
        this.htmlSuccessElement = htmlElement.querySelector('.success-node-icon');
        this.htmlFailureElement = htmlElement.querySelector('.failure-node-icon');
        this.setRoot(isRoot);

        this.onMove = (e: MouseEvent) => {
            this.binaryTree.currentDraggedNode = this;
        };
        this.onEdit = (e: MouseEvent) => {
            e.stopPropagation();
            this.binaryTree.onNodeAction(this, NODE_ACTION.EDIT);
        };
        this.onRemove = (e: MouseEvent) => {
            this.remove();
        }

        this.onSetParent = (e: MouseEvent) => {
            this.setParent(e);
        }
        this.onSetSuccessNode = (e: MouseEvent) => {
            this.setSuccessNode(e);
        }
        this.onSetFailureNode = (e: MouseEvent) => {
            this.setFailureNode(e);
        }
        this.onUnsetParent = (e: MouseEvent) => {
            this.binaryTree.unsetParent(this);
        }
        this.onUnsetSuccessNode = (e: MouseEvent) => {
            this.binaryTree.unsetSuccessNode(this);
        }
        this.onUnsetFailureNode = (e: MouseEvent) => {
            this.binaryTree.unsetFailureNode(this);
        }
        this.onCloneNode = (e: MouseEvent) => {
            this.binaryTree.cloneNode(this);
        }

        this.moveHandler = this.onMove.bind(this);
        this.editHandler = this.onEdit.bind(this);
        this.removeHandler = this.onRemove.bind(this);

        this.setParentHandler = this.onSetParent.bind(this);
        this.setSuccessNodeHandler = this.onSetSuccessNode.bind(this);
        this.setFailureNodeHandler = this.onSetFailureNode.bind(this);

        this.unsetParentHandler = this.onUnsetParent.bind(this);
        this.unsetSuccessNodeHandler = this.onUnsetSuccessNode.bind(this);
        this.unsetFailureNodeHandler = this.onUnsetFailureNode.bind(this);
        this.cloneNodeHandler = this.onCloneNode.bind(this);

        htmlElement.querySelector('.move-node').addEventListener('mousedown', this.moveHandler);
        htmlElement.querySelector('.edit-node').addEventListener('click', this.editHandler, false);
        htmlElement.querySelector('.remove-node').addEventListener('dblclick', this.removeHandler);
        //false to prevent propagation of the click event to document that would reset the current action
        htmlElement.querySelector('.parent-node-icon').addEventListener('click', this.setParentHandler, false);
        htmlElement.querySelector('.success-node-icon').addEventListener('click', this.setSuccessNodeHandler, false);
        htmlElement.querySelector('.failure-node-icon').addEventListener('click', this.setFailureNodeHandler, false);

        htmlElement.querySelector('.parent-node-icon').addEventListener('dblclick', this.unsetParentHandler);
        htmlElement.querySelector('.success-node-icon').addEventListener('dblclick', this.unsetSuccessNodeHandler);
        htmlElement.querySelector('.failure-node-icon').addEventListener('dblclick', this.unsetFailureNodeHandler);
        htmlElement.querySelector('.clone-node').addEventListener('dblclick', this.cloneNodeHandler);
    }

    setName(name: string) {
        this.name = name;
        this.htmlElement.querySelector('.node-name').innerHTML = name;
    }

    setId(id: number) {
        this.id = id;
        this.htmlElement.querySelector('.node-id').innerHTML = id;
    }

    remove(): void {
        this.htmlElement.querySelector('.move-node').removeEventListener('mousedown', this.moveHandler);
        this.htmlElement.querySelector('.edit-node').removeEventListener('click', this.editHandler, false);
        this.htmlElement.querySelector('.remove-node').removeEventListener('dblclick', this.removeHandler);

        this.htmlElement.querySelector('.parent-node-icon').removeEventListener('click', this.setParentHandler, false);
        this.htmlElement.querySelector('.success-node-icon').removeEventListener('click', this.setSuccessNodeHandler, false);
        this.htmlElement.querySelector('.failure-node-icon').removeEventListener('click', this.setFailureNodeHandler, false);

        this.htmlElement.querySelector('.parent-node-icon').removeEventListener('dblclick', this.unsetParentHandler);
        this.htmlElement.querySelector('.success-node-icon').removeEventListener('dblclick', this.unsetSuccessNodeHandler);
        this.htmlElement.querySelector('.failure-node-icon').removeEventListener('dblclick', this.unsetFailureNodeHandler);
        this.htmlElement.querySelector('.clone-node').removeEventListener('dblclick', this.cloneNodeHandler);
        this.htmlElement.remove();

        this.binaryTree.unsetParent(this);
        this.binaryTree.unsetSuccessNode(this);
        this.binaryTree.unsetFailureNode(this);

        this.binaryTree.onNodeRemoved(this);
    }

    updateCloneIcon(): void {
        if(this.canBeCloned() === true) {
            this.htmlElement.querySelector('.clone-node').style.display = 'initial';
        } else  {
            this.htmlElement.querySelector('.clone-node').style.display = 'none';
        }
    }

    setCloned(cloned: boolean): void {
        this.hasBeenCloned = cloned;
        if(this.hasBeenCloned === true) {
            this.htmlElement.querySelector('.success-node-icon').style.display = 'none';
            this.htmlElement.querySelector('.failure-node-icon').style.display = 'none';
        } else {
            this.htmlElement.querySelector('.success-node-icon').style.display = 'initial';
            this.htmlElement.querySelector('.failure-node-icon').style.display = 'initial';
        }
    }

    setParent(e: MouseEvent): void {
        if(this.parentNode === undefined) {
            e.stopPropagation();
            this.binaryTree.onNodeAction(this, NODE_ACTION.SET_PARENT);
        } else if(this.binaryTree.currentAction !== NODE_ACTION.NONE) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'This node already has a parent');
        }
    }

    setSuccessNode(e: MouseEvent): void {
        if(this.successNode === undefined) {
            e.stopPropagation();
            this.binaryTree.onNodeAction(this, NODE_ACTION.SET_SUCCESS);
        } else if(this.binaryTree.currentAction !== NODE_ACTION.NONE) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'This node already has a child on success');
        }
    }

    setFailureNode(e: MouseEvent): void {
        if(this.failureNode === undefined) {
            e.stopPropagation();
            this.binaryTree.onNodeAction(this, NODE_ACTION.SET_FAILURE);
        } else if(this.binaryTree.currentAction !== NODE_ACTION.NONE) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'This node already has a child on failure');
        }
    }

    setZoom(zoom: number) {
        this.currentZoom = zoom;
        this.updateScale();
        this.draw();
    }
    updateScale(): void {
        let transformOrigin = [0, 0];
        var p = ["webkit", "moz", "ms", "o"],
            s = "scale(" + this.currentZoom + ")",
            oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%";

        for (var i = 0; i < p.length; i++) {
            this.htmlElement.style[p[i] + "Transform"] = s;
            this.htmlElement.style[p[i] + "TransformOrigin"] = oString;
        }

        this.htmlElement.style["transform"] = s;
        this.htmlElement.style["transformOrigin"] = oString;
    }

    moveBy(xOffset: number, yOffset: number) {
        if(xOffset !== 0) {
            this.x += xOffset;
        }
        if(yOffset !== 0) {
            this.y += yOffset;
        }
        this.draw();
    }

    setPosition(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    draw(): void {
        let xScreen: number = this.x * this.currentZoom;
        let yScreen: number = this.y * this.currentZoom;
        this.htmlElement.style.left = xScreen + 'px';
        this.htmlElement.style.top = yScreen + 'px';
    }

    isAssociatedWith(node: TreeNode): boolean {
        if(this.parentNode !== undefined && this.parentNode.id === node.id) {
            return true;
        }
        if(this.successNode !== undefined && this.successNode.id === node.id) {
            return true;
        }
        if(this.failureNode !== undefined && this.failureNode.id === node.id) {
            return true;
        }
        if (this.hasBeenCloned === true) {
            //Other node might already be the parent of a copy of this node
            if (node.successNode !== undefined && node.successNode.id === this.id) {
                return true;
            }
            if (node.failureNode !== undefined && node.failureNode.id === this.id) {
                return true;
            }
        }
        return false;
    }

    setRoot(isRoot: boolean): void {
        this.isRoot = isRoot;
        if(isRoot === true) {
            this.htmlParentElement.style.display = 'none';
        } else {
            this.htmlParentElement.style.display = 'initial';
        }
        this.updateCloneIcon();
    }

    canBeCloned(): Boolean {
        return this.isRoot === false && this.successNode === undefined && this.failureNode === undefined;
    }
}