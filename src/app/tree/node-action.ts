export enum NODE_ACTION {
    NONE ='none',
    EDIT = 'edit_node',
    SET_PARENT = 'set_parent',
    SET_SUCCESS = 'set_success',
    SET_FAILURE = 'set_failure'
}