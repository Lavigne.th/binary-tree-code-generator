import { BinaryTreeIpc } from "electron/ipc/binary-tree-ipc";
import { TreeNodeIpc, TreeNodeRelation } from "electron/ipc/tree-node-ipc";
import { BinaryTreeService } from "../service/binary-tree.service";
import { showToast, TOAST_POSITION, TOAST_TYPE } from "../service/toast";
import { NODE_ACTION } from "./node-action";
import { SUPPORTED_SERIALIZATION_LANGUAGE_ENUM } from "../../../electron/ipc/supported-serialization-language-enum";
import { TreeNode } from "./tree-node";

export class BinaryTree {

    private binaryTreeService: BinaryTreeService;

    savePath: string;

    treeName: string;

    root?: TreeNode;

    nodes: Array<TreeNode> = [];

    apiRelativePath: string;

    apiClassName: string;

    currentDraggedNode?: TreeNode;

    currentFocusedNode?: TreeNode;

    currentAction: NODE_ACTION = NODE_ACTION.NONE;

    currentMaxId: number = 1;

    serializationLanguage: SUPPORTED_SERIALIZATION_LANGUAGE_ENUM;

    clones: Map<number, Array<TreeNode>> = new Map();
    
    constructor(binaryTreeService: BinaryTreeService, savePath: string, treeName: string, apiRelativePath: string, apiClassName: string, serializationLanguage: SUPPORTED_SERIALIZATION_LANGUAGE_ENUM) {
        this.binaryTreeService = binaryTreeService;
        this.savePath = savePath;
        this.treeName = treeName;
        this.apiRelativePath = apiRelativePath;
        this.apiClassName = apiClassName;
        this.serializationLanguage = serializationLanguage;
    }

    addNode(htmlElement: any, currentZoom: number) {
        this.nodes.push(new TreeNode(this, this.currentMaxId++, false, 'NoName', htmlElement, 100, 100, currentZoom, false));
    }

    onNodeRemoved(node: TreeNode) {
        let index: number = this.nodes.indexOf(node);
        if(index >= 0) {
            if(this.currentFocusedNode?.id === node.id) {
                this.currentFocusedNode = undefined;
                this.currentAction = NODE_ACTION.NONE;
            }
            this.nodes.splice(index, 1);
        }
        if(node.hasBeenCloned === true) {
            let allClones: Array<TreeNode> = <Array<TreeNode>> this.clones.get(node.id);
            allClones.splice(allClones.indexOf(node), 1);
            if(allClones.length === 1) {
                allClones[0].setCloned(false);
                this.clones.delete(node.id);
            }
        }
    }

    /**
     * If currentAction is undefined or the new action is edit => set new action as current
     * Otherwise a new relation should be created and it depends what was the previous node et action selected
     * @param node  
     * @param action 
     */
    onNodeAction(node: TreeNode, action: NODE_ACTION) {
        if(this.currentFocusedNode === undefined || action === NODE_ACTION.EDIT) {
            this.currentFocusedNode = node;
            this.currentAction = action;
        } else if(this.isCurrentRelationAction()) {
            if(node.isAssociatedWith(this.currentFocusedNode) === false) {
                if(action === NODE_ACTION.SET_SUCCESS) {
                    this.trySetSuccessNodeRelation(node);
                } else if(action === NODE_ACTION.SET_FAILURE) {
                    this.trySetFailureNodeRelation(node);
                } else if(action === NODE_ACTION.SET_PARENT) {
                    this.trySetParentNodeRelation(node);
                }
            } else {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'These node are already associated');
            }
            node.updateCloneIcon();
            this.currentFocusedNode.updateCloneIcon();
            this.currentAction = NODE_ACTION.NONE;
            this.currentFocusedNode = undefined;
            this.binaryTreeService.onBinaryTreeRelationChanged.emit();
        }
        this.binaryTreeService.onBinaryTreeActionChanged.emit();
    }

    cloneNode(node: TreeNode) {
        if(node.canBeCloned() === true) {
            node.setCloned(true);

            let cloneHtml: any = document.querySelector('.clone.node')?.cloneNode(true);
            cloneHtml.classList.remove('clone');
            document.querySelector('main')?.appendChild(cloneHtml);
            let clone: TreeNode = new TreeNode(this, node.id, false, node.name, cloneHtml, 100, 100, node.currentZoom, true);
            this.nodes.push(clone);
            if(this.clones.has(node.id) === false) {
                this.clones.set(node.id, []);
                this.clones.get(node.id)?.push(node);
            }
            this.clones.get(node.id)?.push(clone);
        }
    }

    /**
     * Current Action must be parent
     * Child node must not have a parent
     * Target node must not have a failure node
     * @param node 
     */
    trySetFailureNodeRelation(node: TreeNode) {
        if(this.currentFocusedNode) {
            if(node.id === this.currentFocusedNode.id) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'A node can\'t be associated with himself');
                return;
            }
            if(this.currentAction !== NODE_ACTION.SET_PARENT) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Can only set failure child relation with parent');
                return;
            }
            if(this.currentFocusedNode.parentNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Parent node is alrteady set');
                return;
            }
            if(node.failureNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Targeted node already has a failure node child');
                return;
            }
            this.setFailureNodeRelation(node, this.currentFocusedNode);
        }
    }

    /**
     * Current Action must be parent
     * Child node must not have a parent
     * Target node must not have a success node
     * @param node 
     */
    trySetSuccessNodeRelation(node: TreeNode) {
        if(this.currentFocusedNode) {
            if(node.id === this.currentFocusedNode.id) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'A node can\'t be associated with himself');
                return;
            }
            if(this.currentAction !== NODE_ACTION.SET_PARENT) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Can only set Success child relation with parent');
                return;
            }
            if(this.currentFocusedNode.parentNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Parent node is alrteady set');
                return;
            }
            if(node.successNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Targeted node already has a success node child');
                return;
            }
            this.setSuccessNodeRelation(node, this.currentFocusedNode);
        }
    }

    setSuccessNodeRelation(parent: TreeNode, child: TreeNode) {
        parent.successNode = child;
        child.parentNode = parent;
    }

    setFailureNodeRelation(parent: TreeNode, child: TreeNode) {
        parent.failureNode = child;
        child.parentNode = parent;
    }

    /**
      * Current Action must be success or failure
      * Child node must not have a parent
      * Parent must not have a child on the targeted relation (success or failure)
      * @param node 
      */
    trySetParentNodeRelation(node: TreeNode) {
        if (this.currentFocusedNode) {
            if (node.id === this.currentFocusedNode.id) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'A node can\'t be associated with himself');
                return;
            }
            if (this.currentAction !== NODE_ACTION.SET_FAILURE && this.currentAction !== NODE_ACTION.SET_SUCCESS) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Can only set Parent relation with either success or failure of child node');
                return;
            }
            if (this.currentAction === NODE_ACTION.SET_FAILURE && this.currentFocusedNode.failureNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Parent node already has child on the failure spot');
                return;
            }
            if (this.currentAction === NODE_ACTION.SET_SUCCESS && this.currentFocusedNode.successNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Parent node already has child on the success spot');
                return;
            }
            if (node.parentNode !== undefined) {
                showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Child node already has a parent.');
                return;
            }
            node.parentNode = this.currentFocusedNode;
            if (this.currentAction === NODE_ACTION.SET_SUCCESS) {
                this.currentFocusedNode.successNode = node;
            } else {
                this.currentFocusedNode.failureNode = node;
            }
        }
    }


    unsetParent(node: TreeNode) {
        if(node.parentNode) {
            let parentNode: TreeNode = node.parentNode;
            if(node.parentNode.successNode?.instanceId === node.instanceId) {
                node.parentNode.successNode = undefined;
                node.parentNode = undefined;
            } else if(node.parentNode.failureNode?.instanceId === node.instanceId) {
                node.parentNode.failureNode = undefined;
                node.parentNode = undefined;
            }
            parentNode.updateCloneIcon();
        }
        this.binaryTreeService.onBinaryTreeRelationChanged.emit();
    }

    unsetSuccessNode(node: TreeNode) {
        if(node.successNode) {
            node.successNode.parentNode = undefined;        
            node.successNode = undefined;
        }
        node.updateCloneIcon();
        this.binaryTreeService.onBinaryTreeRelationChanged.emit();
    }

    unsetFailureNode(node: TreeNode) {
        if(node.failureNode) {
            node.failureNode.parentNode = undefined;        
            node.failureNode = undefined;
        }
        node.updateCloneIcon();
        this.binaryTreeService.onBinaryTreeRelationChanged.emit();
    }

    isCurrentRelationAction(): boolean {
        return this.currentAction === NODE_ACTION.SET_FAILURE || 
            this.currentAction === NODE_ACTION.SET_PARENT || 
            this.currentAction === NODE_ACTION.SET_SUCCESS;
    }

    changeNodeName(node: TreeNode, name: string) {
        for(let nodeOrClone of this.nodes.filter(n => n.id === node.id)) {
            nodeOrClone.setName(name);
        }
        
        this.currentAction = NODE_ACTION.NONE;
        this.currentFocusedNode = undefined;
        this.binaryTreeService.onBinaryTreeActionChanged.emit();
    }

    setRoot(): void {
        if(this.root !== undefined) {
            this.root.setRoot(false);
        }
        if(this.currentAction === NODE_ACTION.EDIT) {
            if(this.currentFocusedNode) {
                if(this.currentFocusedNode.parentNode) {
                    this.unsetParent(this.currentFocusedNode);
                    showToast(TOAST_TYPE.WARNING, TOAST_POSITION.TOP_CENTER, 'Removed parent node of ' + this.currentFocusedNode.id + '. Root node can\'t have a parent.');
                }
                this.currentFocusedNode.setRoot(true);
                this.root = this.currentFocusedNode;
            }
        }
    }

    isReadyForSave(): boolean {
        if(this.root === undefined) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Tree doesn\t have a root.');
            return false;
        }
        return this.checkNode(this.root);
    }

    checkNode(node: TreeNode): boolean {
        if(node.isRoot === true && node.parentNode !== undefined) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Root node ' + node.id + ' can\'t have a parent.');
            return false;
        } 
        if(node.successNode !== undefined && node.failureNode === undefined) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Branch node ' + node.id + ' doesn\'t have a failure node');
            return false;
        }
        if(node.failureNode !== undefined && node.successNode === undefined) {
            showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, 'Branch node ' + node.id + ' doesn\'t have a success node');
            return false;
        }
        //Node is branch with 2 children
        if(node.successNode !== undefined && node.failureNode != undefined) {
            return this.checkNode(node.successNode) === true && this.checkNode(node.failureNode) === true;
        } 
        //Here node is a leaf and is ok
        return true;
    }

    toIpc(currentZoom: number): BinaryTreeIpc {
        let nodesIpc: Array<TreeNodeIpc> = [];
        for(let node of this.nodes) {
            let parentNode: TreeNodeRelation | undefined = node.parentNode !== undefined ? new TreeNodeRelation(node.parentNode.id, node.parentNode.instanceId) : undefined;
            let successNode: TreeNodeRelation | undefined = node.successNode !== undefined ? new TreeNodeRelation(node.successNode.id, node.successNode.instanceId) : undefined;
            let failureNode: TreeNodeRelation | undefined = node.failureNode !== undefined ? new TreeNodeRelation(node.failureNode.id, node.failureNode.instanceId) : undefined;
            nodesIpc.push(new TreeNodeIpc(node.id, node.instanceId, node.name, node.x, node.y, parentNode, successNode, failureNode, node.hasBeenCloned));
        }
        return new BinaryTreeIpc(this.savePath, this.treeName, <number>this.root?.id, this.apiRelativePath, this.apiClassName, nodesIpc, currentZoom, this.serializationLanguage);
    }
    
    destroy() {
        for(let i = this.nodes.length - 1; i >= 0; i--) {
            this.nodes[i].remove();
        }
    }
  
}


