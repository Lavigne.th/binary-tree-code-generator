import { Injectable } from '@angular/core';
import { IPC_CHANNEL } from 'electron/ipc/ipc-channel';
import { IpcLog } from 'electron/ipc/ipc-log';
import { showToast, TOAST_POSITION, TOAST_TYPE } from './toast';

declare var electron: any;

@Injectable({
  providedIn: 'root'
})
export class IpcService {

  constructor() {
    this.getipc().on(IPC_CHANNEL.LOG, (e: any, log: IpcLog) => {
      console.log(log.level + ' ' + log.message);
    });
   }

   getipc(): any {
    return electron.ipcRenderer;
  }

  send1(channel: IPC_CHANNEL | string, data: any) {
    electron.ipcRenderer.send(channel, data);
  }

  send2(channel: IPC_CHANNEL | string, data1: any, data2: any) {
    electron.ipcRenderer.send(channel, data1, data2);
  }
}
