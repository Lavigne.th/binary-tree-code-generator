import { Injectable } from '@angular/core';
import { TreeNode } from '../tree/tree-node';
import { BinaryTreeService } from './binary-tree.service';

@Injectable({
  providedIn: 'root'
})
export class CanvasService {

  width: number = 0;
  height: number = 0;

  htmlCanvas: any;
  ctx: any;

  constructor(private binaryTreeService: BinaryTreeService) {
    binaryTreeService.renderScene.subscribe(() => {
      this.render();
    });    
   }

  resizeCanvasSize(): void {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.htmlCanvas.setAttribute('width', this.width);
    this.htmlCanvas.setAttribute('height', this.height);
  }

  render(): void {
    this.ctx.clearRect(0, 0, this.width, this.height);
    if(this.binaryTreeService.currentTree) {
      for(let node of this.binaryTreeService.currentTree.nodes) {
        if(node.successNode) {
          this.drawLineBetweenElements(node.htmlSuccessElement, node.successNode.htmlParentElement);
        }
        if(node.failureNode) {
          this.drawLineBetweenElements(node.htmlFailureElement, node.failureNode.htmlParentElement);
        }
      }
    }
  }

  init(): void {
    this.htmlCanvas = document.querySelector('#mainCanvas');
    this.ctx = this.htmlCanvas.getContext('2d');
  }

  drawLineBetweenElements(elem1: any, elem2: any) {
    let rec1: any = elem1.getBoundingClientRect();
    let rec2: any = elem2.getBoundingClientRect();
    let x1: number = rec1.x + (rec1.width / 2);
    let y1: number = rec1.y + (rec1.height / 2);

    let x2: number = rec2.x + (rec2.width / 2);
    let y2: number = rec2.y + (rec2.height / 2);

    this.ctx.beginPath();
    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.stroke(); 
  }

  isVisible(node: TreeNode): boolean {
    return true;
  }
}

(<any>window).drawLine = function drawLine(x1: number, y1: number, x2: number, y2: number) {
  var c: any = document.getElementById("mainCanvas");
  var ctx = c.getContext("2d");
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke(); 
}