import { EventEmitter, Injectable } from '@angular/core';
import { BinaryTreeIpc } from 'electron/ipc/binary-tree-ipc';
import { FolderChosenIpc } from 'electron/ipc/folder-chosen-ipc';
import { GenericRequestwithIdIpc } from 'electron/ipc/generic-request-with-id-ipc';
import { IPC_CHANNEL } from 'electron/ipc/ipc-channel';
import { CreateBinaryTreeRequest } from '../component/create-project-modal/create-binary-tree-request';
import { OPEN_TREE_CONF_MODAL_ENUM } from '../component/create-project-modal/open-tree-conf-modal-modal-enum';
import { BinaryTree } from '../tree/binary-tree';
import { NODE_ACTION } from '../tree/node-action';
import { SUPPORTED_SERIALIZATION_LANGUAGE_ENUM } from '../../../electron/ipc/supported-serialization-language-enum';
import { TreeNode } from '../tree/tree-node';
import { CanvasService } from './canvas.service';
import { IpcService } from './ipc.service';

@Injectable({
  providedIn: 'root'
})
export class BinaryTreeService {

  currentTree?: BinaryTree;

  lastChoseFolderRequest?: GenericRequestwithIdIpc;

  onBinaryTreeCreated: EventEmitter<any> = new EventEmitter();

  onBinaryTreeActionChanged: EventEmitter<any> = new EventEmitter();

  onBinaryTreeRelationChanged: EventEmitter<any> = new EventEmitter();

  onSetCurrentZoom: EventEmitter<number> = new EventEmitter();

  renderScene: EventEmitter<any> = new EventEmitter();

  changeZoom: EventEmitter<number> = new EventEmitter();

  openTreeConfModalMode: OPEN_TREE_CONF_MODAL_ENUM = OPEN_TREE_CONF_MODAL_ENUM.CREATE_NEW_TREE;

  constructor(private ipc: IpcService) { 
    ipc.getipc().on(IPC_CHANNEL.LOAD_TREE_RESULT, (e: any, tree: BinaryTreeIpc) => {
      this.onBinaryTreeResult(tree);
      this.changeZoom.emit(tree.currentZoom);
    });
  }

  createBinaryTree(request: CreateBinaryTreeRequest) {
    if(this.currentTree) {
      this.currentTree.destroy();
    }
    this.currentTree = new BinaryTree(this, <string>request.saveFolderPath, <string>request.treeName, <string>request.apiRelativePath, <string>request.apiClassName, <SUPPORTED_SERIALIZATION_LANGUAGE_ENUM>request.serializationLanguage);
    this.onBinaryTreeCreated.emit();
  }

  addNode(htmlElement: any, currentZoom: number) {
    if(this.currentTree) {
      this.currentTree.addNode(htmlElement, currentZoom);
    } else {
      console.log('No tree to add node...');
    }
  }

  isDragging(): boolean {
    return this.currentTree !== undefined && this.currentTree.currentDraggedNode !== undefined;
  }

  stopDragging(): void {
    if(this.currentTree) {
      this.currentTree.currentDraggedNode = undefined;
    }
  }

  resetNodeFocus(): void {
    if(this.currentTree && this.currentTree.currentAction !== NODE_ACTION.NONE) {
      this.currentTree.currentAction = NODE_ACTION.NONE;
      this.currentTree.currentFocusedNode = undefined;
      this.onBinaryTreeActionChanged.emit();
    }
  }

  changeNodeName(node: TreeNode | undefined, name: string) {
    if(this.currentTree && node) {
      this.currentTree.changeNodeName(node, name);
    }
  }

  saveTree(currentZoom: number) {
    if(this.currentTree && this.currentTree.isReadyForSave()) {
      this.ipc.send1(IPC_CHANNEL.SAVE_BINARY_TREE, this.currentTree.toIpc(currentZoom));
    }
  }

  setRootNode() {
    if(this.currentTree) {
      this.currentTree.setRoot();
    }
  }

  loadTree() {
    if(this.currentTree) {
      this.currentTree.destroy();
    }
    this.ipc.send1(IPC_CHANNEL.LOAD_BINARY_TREE, null);
  }

  onBinaryTreeResult(tree: BinaryTreeIpc) {
    this.onSetCurrentZoom.emit(tree.currentZoom);
    this.currentTree = new BinaryTree(this, <string>tree.saveFolderPath, <string>tree.treeName, <string>tree.apiRelativePath, <string>tree.apiClassName, tree.serializationLanguage);
    this.currentTree.currentMaxId = 0;
    this.createNodes(tree);
    this.bindNodes(tree);
    this.currentTree.root = this.currentTree.nodes.find((n) => n.id === tree.rootNodeId);
    this.renderScene.emit();
  }

  bindNodes(tree: BinaryTreeIpc) {
    if(this.currentTree) {
      for(let node of tree.nodes) {
        let childNode: TreeNode | undefined = this.currentTree.nodes.find((n) => n.serializedInstanceId === node.instanceId);
        if(node.parentNode !== undefined) {
          let parentNode: TreeNode | undefined = this.currentTree.nodes.find((n) => n.serializedInstanceId === node.parentNode?.instanceId);
          let successRelation: boolean = tree.nodes.find((n) => n.instanceId === node.parentNode?.instanceId)?.successNode?.instanceId === node.instanceId;
          if(parentNode && childNode) {
            if(successRelation === true) {
              this.currentTree.setSuccessNodeRelation(parentNode, childNode);
            } else {
              this.currentTree.setFailureNodeRelation(parentNode, childNode);
            }
            parentNode.updateCloneIcon();
          } else {
            new Error("No node found");
          }
        }
      }
    }
  }

  createNodes(tree: BinaryTreeIpc): void {
    if(this.currentTree) {
      for(let node of tree.nodes) {
        let clone: any = document.querySelector('.clone.node')?.cloneNode(true);
        clone.classList.remove('clone');
        let treeNode: TreeNode = new TreeNode(
          this.currentTree,
          node.id,
          node.id === tree.rootNodeId,
          node.name,
          clone,
          node.x,
          node.y,
          tree.currentZoom,
          node.hasBeenCloned
        );
        treeNode.serializedInstanceId = node.instanceId;
        this.currentTree.nodes.push(treeNode);
        document.querySelector('main')?.appendChild(clone);
        if(node.hasBeenCloned === true) {
          if(this.currentTree.clones.has(node.id) === false) {
            this.currentTree.clones.set(node.id, []);
          }
          this.currentTree.clones.get(node.id)?.push(treeNode);
        }
        if(node.id >= this.currentTree.currentMaxId) {
          this.currentTree.currentMaxId = node.id + 1;
        }
      }
    }
  }
}
