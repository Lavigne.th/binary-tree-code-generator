declare var $: any;

export enum TOAST_TYPE {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning"
}

export enum TOAST_POSITION {
  TOP_CENTER = 'top center'
}

export function showToast(toastType: TOAST_TYPE, toastPosition: TOAST_POSITION, text: string) {
  $('body').toast({
    position: toastPosition.toString(),
    class: toastType.toString(),
    message: text
  });
}