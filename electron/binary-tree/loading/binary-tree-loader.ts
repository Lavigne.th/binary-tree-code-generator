import * as fs from "fs"
import { app, BrowserWindow, ipcMain, ipcRenderer, dialog, IpcMainEvent } from 'electron'
import * as ts from 'typescript'
import { IPC_CHANNEL } from '../../ipc/ipc-channel';
import { CONSTANTS } from "../constant";
import { LOAD_TREE_FAILURE_REASON } from "./load-tree-failure-reason";
import { BinaryTreeIpc } from "../../ipc/binary-tree-ipc";

export class BinaryTreeLoader {

  mainWindow: BrowserWindow;

  constructor(mainWindow: BrowserWindow) {
    this.mainWindow = mainWindow;

    ipcMain.on(IPC_CHANNEL.LOAD_BINARY_TREE, (e: IpcMainEvent, params: any) => {
      this.loadAsync();
  });
  }

  loadAsync(): void {
    dialog.showOpenDialog({
      properties:['openFile'],
      filters:[
        {name: CONSTANTS.TREE_CONFIGURATION_FILE_NAME, extensions:['json']}
      ]
    }).then((value: Electron.OpenDialogReturnValue) => {
      if(value.canceled === false && value.filePaths.length > 0) {
        let pathArray: Array<string> = value.filePaths[0].split('/');
        let file: string = pathArray[pathArray.length - 1];
        if(file === CONSTANTS.TREE_CONFIGURATION_FILE) {
          const fileContents: string = fs.readFileSync(value.filePaths[0]).toString()
          let binaryTree: BinaryTreeIpc = <BinaryTreeIpc> JSON.parse(fileContents);
          pathArray.splice(pathArray.length - 1, 1);
          binaryTree.saveFolderPath = pathArray.join('/');
          this.mainWindow.webContents.send(IPC_CHANNEL.LOAD_TREE_RESULT, binaryTree);
        } else {
          this.mainWindow.webContents.send(IPC_CHANNEL.LOAD_TREE_FAILED, LOAD_TREE_FAILURE_REASON.WRONG_SELECTED_FILE_TYPE);
        }
      }
    });
  }
}