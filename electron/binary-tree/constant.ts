
export enum CONSTANTS {
    TREE_CONFIGURATION_FILE = 'binary-tree.json',
    TREE_CONFIGURATION_FILE_NAME = 'binary-tree',
    TREE_NODES_SAVE_FOLDER = 'nodes',
}