import * as ts from 'typescript'
import * as fs from 'fs'
import { ILogger } from '../../../logger/ilogger';

export class TsReflection {

    private logger: ILogger;

    program: ts.Program;

    sourceFile: ts.SourceFile;

    fileContents: string;

    classRootNode: ts.ClassDeclaration;

    isLoaded: boolean = false;

    typeChecker: ts.TypeChecker;

    constructor(logger: ILogger) {
        this.logger = logger;
    }

    loadTsfile(filePath: string): boolean {
        this.reset();
        if (fs.existsSync(filePath) === true) {
            this.program = ts.createProgram([filePath], { allowJs: true });
            this.sourceFile = this.program.getSourceFile(filePath);
            //For some reason I had to get the TypeChecker otherwise I couldn't call functions on nodes without specifying the sourceFile
            this.typeChecker = this.program.getTypeChecker();
            this.fileContents = fs.readFileSync(filePath).toString();
            ts.forEachChild(this.sourceFile, node => {
                if (ts.isClassDeclaration(node)) {
                    this.classRootNode = node;
                }
            });
            this.isLoaded = true;
            return true;
        }
        this.logger.logError(`No file for reflection as ${filePath}`);
        return false;
    }

    reset(): void {
        this.isLoaded = false;
        this.program = undefined;
        this.sourceFile = undefined;
        this.fileContents = undefined;
        this.classRootNode = undefined;
        this.typeChecker = undefined;
    }

    getImports(): Array<ts.ImportDeclaration> {
        let imports: Array<ts.ImportDeclaration> = [];
        ts.forEachChild(this.sourceFile, node => {
            if (ts.isImportDeclaration(node)) {
                imports.push(node);
            }
        });
        return imports;
    }

    getClassProperties(): Array<ts.PropertyDeclaration> {
        let classProperties: Array<ts.PropertyDeclaration> = [];
        this.classRootNode.members.forEach((member) => {
            if (ts.isPropertyDeclaration(member)) {
                classProperties.push(member);
            }
        });
        return classProperties;
    }

    getClassMethods(): Array<ts.MethodDeclaration> {
        let classMethods: Array<ts.MethodDeclaration> = [];
        this.classRootNode.members.forEach((member) => {
            if (ts.isMethodDeclaration(member)) {
                classMethods.push(member);
            }
        });
        return classMethods;
    }


    readCode(startIndex: number, endIndex: number): string {
        if (this.isLoaded === true) {
            return this.fileContents.substring(startIndex, endIndex);
        }
        return '';
    }
}