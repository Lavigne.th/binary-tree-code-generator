import ts = require("typescript");
import { BinaryTreeIpc } from "../../../ipc/binary-tree-ipc";
import { TreeNodeIpc } from "../../../ipc/tree-node-ipc";
import { ILogger } from "../../../logger/ilogger";
import { ATreeClassInitializer } from "../atree-class-initializer";
import { AbstractSerializationClass } from "../abstract-class/abstract-serialization-class";
import { ClassConstructor } from "../abstract-class/class-constructor";
import { ClassMethod } from "../abstract-class/class-function";
import { ClassProperty } from "../abstract-class/class-property";
import { FunctionParameter } from "../abstract-class/function-parameter";
import { TsReflection } from "./ts-reflection";
import { AbstractNodeClass } from "../abstract-class/abstract-node-class";
import { AbstractTreeClass } from "../abstract-class/abstract-tree-class";
import { CONSTANTS } from "../../constant";

export class TsClassInitializer extends ATreeClassInitializer {

    public static BRANCH_FUNCTION: string = 'evaluate';

    public static LEAF_FUNCTION: string = 'execute';

    public static API_PROPERTY_NAME: string = 'api';

    public static SUCCESS_PROPERTY_NAME: string = 'success';

    public static FAILURE_PROPERTY_NAME: string = 'failure';

    public static NODE_TYPE_PROPERTY_NAME: string = '_type';

    public static ROOT_NODE_PROPERTY_NAME: string = 'root';

    public static RUN_TREE_FUNCTION: string = 'run';

    private tsReflection: TsReflection;
    
    constructor(logger: ILogger) {
        super(logger, 'ts');
        this.tsReflection = new TsReflection(logger);
    }

    loadNode(node: AbstractNodeClass, currentTree: BinaryTreeIpc, previousTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc, failureNode: TreeNodeIpc): void {
        let existingNode: TreeNodeIpc = previousTree.nodes.find(n => n.id === node.ipcNode.id);
        if(existingNode !== undefined) {
            let nodeFilePath: string = `${currentTree.saveFolderPath}/${existingNode.saveFile}`;
            this.tsReflection.loadTsfile(nodeFilePath);
            this.loadClassImports(node, currentTree, previousTree);
            this.loadClassProperties(node);
            this.loadClassMethods(node);        }
    }

    private loadClassProperties(node: AbstractNodeClass): void {
        for(let prop of this.tsReflection.getClassProperties()) {
            let propName: string = prop.name.getText();
            if(propName === TsClassInitializer.API_PROPERTY_NAME) {
                continue;
            }
            if(propName === TsClassInitializer.SUCCESS_PROPERTY_NAME) {
                continue;
            }
            if(propName === TsClassInitializer.FAILURE_PROPERTY_NAME) {
                continue;
            }
            if(propName === TsClassInitializer.NODE_TYPE_PROPERTY_NAME) {
                continue;
            }
            if(prop.initializer !== undefined) {
                node.properties.push(new ClassProperty(propName, prop.type.getText(), prop.initializer.getText()));
            } else {
                node.properties.push(new ClassProperty(propName, prop.type.getText(), undefined));
            }
        }
    }

    private loadClassMethods(node: AbstractNodeClass): void {
        for(let method of this.tsReflection.getClassMethods()) {
            let code: string = method.body.getText();
            let existingMethod: ClassMethod | undefined = node.methods.find(f => f.name === method.name.getText());
            if(existingMethod !== undefined) {
                //Should either be the eval() or execute() functions generated previously
                existingMethod.code = code;
            } else {
                let returnType: string | undefined = method.type === undefined ? undefined : method.type.getText();
                let newMethod: ClassMethod = new ClassMethod(method.name.getText(), returnType, code);
                for(let param of method.parameters) {
                    let paramType: string = param.type === undefined ? 'any': param.type.getText();
                    newMethod.params.push(new FunctionParameter(param.name.getText(), paramType));
                }
                node.methods.push(newMethod);
            }
        }
    }

    /**
     * Only add imports that are :
     * - not the previous api
     * - the current api
     * - looking like its a node import (split '-' ending with number)
     * @param node 
     * @param currentTree 
     * @param previousTree 
     */
    private loadClassImports(node: AbstractNodeClass, currentTree: BinaryTreeIpc, previousTree: BinaryTreeIpc) {
        for(let _import of this.tsReflection.getImports()) {
            let importPath: any = _import.moduleSpecifier.getText();
            if(importPath.includes(currentTree.apiRelativePath) || importPath.includes(previousTree.apiRelativePath)) {
                continue;
            }
            let arr: Array<string> = importPath.split('-');
            let endNumber: any = parseInt(arr[arr.length - 1]);
            if(isNaN(endNumber) === false) {
                continue;
            }
            node.imports.push(this.tsReflection.readCode(_import.getStart(), _import.getEnd()));
        }
    }
    
    initializeNode(node: AbstractNodeClass, binaryTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc, failureNode: TreeNodeIpc): void {
        node.className = TsClassInitializer.getClassNameForNode(node.ipcNode);
        this.setNodeImports(node, binaryTree, isBranch, successNode, failureNode);
        this.setNodeProperties(node, binaryTree, isBranch, successNode, failureNode);
        this.setNodeConstructor(node, binaryTree, isBranch, successNode, failureNode);
        this.setNodeFunctions(node, binaryTree, isBranch, successNode, failureNode);
    }

    setNodeFunctions(node: AbstractNodeClass, binaryTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc, failureNode: TreeNodeIpc) {
        if(isBranch === true) {
            node.methods.push(new ClassMethod(TsClassInitializer.BRANCH_FUNCTION, 'boolean', ' { }'));
        } else {
            node.methods.push(new ClassMethod(TsClassInitializer.LEAF_FUNCTION, 'void', ' { }'));
        }
    }

    setNodeImports(node: AbstractNodeClass, binaryTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc, failureNode: TreeNodeIpc) {
        node.imports.push(`import { ${binaryTree.apiClassName} } from "${binaryTree.apiRelativePath}"`);
        if(isBranch === true) {
            node.imports.push(`import { ${TsClassInitializer.getClassNameForNode(successNode)} } from "./${this.getFileNameForNode(successNode)}"`);
            node.imports.push(`import { ${TsClassInitializer.getClassNameForNode(failureNode)} } from "./${this.getFileNameForNode(failureNode)}"`);
        }
    }

    setNodeProperties(node: AbstractNodeClass, binaryTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc, failureNode: TreeNodeIpc) {
        node.properties.push(new ClassProperty(TsClassInitializer.API_PROPERTY_NAME, binaryTree.apiClassName, undefined));
        if (isBranch === true) {
            node.properties.push(new ClassProperty(TsClassInitializer.SUCCESS_PROPERTY_NAME, TsClassInitializer.getClassNameForNode(successNode), undefined));
            node.properties.push(new ClassProperty(TsClassInitializer.FAILURE_PROPERTY_NAME, TsClassInitializer.getClassNameForNode(failureNode), undefined));
            node.properties.push(new ClassProperty(TsClassInitializer.NODE_TYPE_PROPERTY_NAME, 'string', "'branch'"));
        } else {
            node.properties.push(new ClassProperty(TsClassInitializer.NODE_TYPE_PROPERTY_NAME, 'string', "'leaf'"));
        }
    }

    private setNodeConstructor(node: AbstractNodeClass, binaryTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc, failureNode: TreeNodeIpc) {
        let code: string = `{\n      this.${TsClassInitializer.API_PROPERTY_NAME} = ${TsClassInitializer.API_PROPERTY_NAME};\n`;
        if(isBranch === true) {
            code += `      this.${TsClassInitializer.SUCCESS_PROPERTY_NAME} = new ${TsClassInitializer.getClassNameForNode(successNode)}(${TsClassInitializer.API_PROPERTY_NAME});\n`
            code += `      this.${TsClassInitializer.FAILURE_PROPERTY_NAME} = new ${TsClassInitializer.getClassNameForNode(failureNode)}(${TsClassInitializer.API_PROPERTY_NAME});\n`
        }
        code += '   }'
        node.classConstructor = new ClassConstructor(code);
        node.classConstructor.params.push(new FunctionParameter(TsClassInitializer.API_PROPERTY_NAME, binaryTree.apiClassName));
    }

    static getClassNameForNode(ipcNode: TreeNodeIpc) {
        return `${ipcNode.name}${ipcNode.id}`; 
    }

    static getClassNameForTree(binaryTree: BinaryTreeIpc) {
        return `${binaryTree.treeName}Tree`; 
    }

    getFileNameForNode(ipcNode: TreeNodeIpc): string {
        return `${this.getFileNameFromName(ipcNode.name)}-${ipcNode.id}`;
    }

    getFileNameForTree(binaryTree: BinaryTreeIpc): string {
        return `${this.getFileNameFromName(binaryTree.treeName)}-tree`;
    }

    private getFileNameFromName(name: string): string {
        let result = '';
        for (var i = 0; i < name.length; i++) {
            if(this.isLowerCase(name[i]) === false) {
                if(i > 0) {
                    result += `-${name[i].toLocaleLowerCase()}`;
                } else {
                    result += name[i].toLocaleLowerCase();
                }
            } else {
                result += name[i];
            }
        }
         
        return result;
    }

    private isLowerCase(str): boolean
    {
        return str === str.toLowerCase() && str !== str.toUpperCase();
    }

    initializeTree(tree: AbstractTreeClass, rootNode: TreeNodeIpc): void {
        tree.className = TsClassInitializer.getClassNameForTree(tree.binaryTreeIpc);
        this.setTreeImports(tree, rootNode);
        this.setTreeProperties(tree, rootNode);
        this.setTreeConstructor(tree, rootNode);
        this.setTreeFunctions(tree, rootNode);
    }

    private setTreeImports(tree: AbstractTreeClass, rootNode: TreeNodeIpc) {
        tree.imports.push(`import { ${tree.binaryTreeIpc.apiClassName} } from "${tree.binaryTreeIpc.apiRelativePath}"`);
        tree.imports.push(`import { ${TsClassInitializer.getClassNameForNode(rootNode)} } from "./${CONSTANTS.TREE_NODES_SAVE_FOLDER}/${this.getFileNameForNode(rootNode)}"`);
    }

    private setTreeProperties(tree: AbstractTreeClass, rootNode: TreeNodeIpc) {
        tree.properties.push(new ClassProperty(TsClassInitializer.ROOT_NODE_PROPERTY_NAME, TsClassInitializer.getClassNameForNode(rootNode), undefined));
        tree.properties.push(new ClassProperty(TsClassInitializer.API_PROPERTY_NAME, tree.binaryTreeIpc.apiClassName, undefined));
    }

    private setTreeConstructor(tree: AbstractTreeClass, rootNode: TreeNodeIpc) {
        let code: string = `{\n      this.${TsClassInitializer.API_PROPERTY_NAME} = ${TsClassInitializer.API_PROPERTY_NAME};\n`;
        code += `      this.${TsClassInitializer.ROOT_NODE_PROPERTY_NAME} = new ${TsClassInitializer.getClassNameForNode(rootNode)}(${TsClassInitializer.API_PROPERTY_NAME});\n`;
        code += '   }'
        tree.classConstructor = new ClassConstructor(code);
        tree.classConstructor.params.push(new FunctionParameter(TsClassInitializer.API_PROPERTY_NAME, tree.binaryTreeIpc.apiClassName));
    }

    private setTreeFunctions(tree: AbstractTreeClass, rootNode: TreeNodeIpc) {
        let code: string = ' {\n';
        code += `      let currentNode: any = this.${TsClassInitializer.ROOT_NODE_PROPERTY_NAME};\n`
        code += `      while(currentNode._type === 'branch') {\n`
        code += `         if(currentNode.evaluate() === true) {\n`
        code += `            currentNode = currentNode.success;\n`
        code += `         } else {\n`
        code += `            currentNode = currentNode.failure;\n`
        code += `         }\n`
        code += `      }\n`
        code += `      currentNode.execute();\n`
        code += '   }\n';

        tree.methods.push(new ClassMethod(TsClassInitializer.RUN_TREE_FUNCTION, 'void', code));
    }
}