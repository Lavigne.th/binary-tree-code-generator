import { ILogger } from "../../../logger/ilogger";
import { ATreeSerializer } from "../atree-serializer";
import { AbstractSerializationClass } from "../abstract-class/abstract-serialization-class";
import * as fs from 'fs'
import { FunctionParameter } from "../abstract-class/function-parameter";
import { BinaryTreeIpc } from "../../../ipc/binary-tree-ipc";
import { AbstractTreeClass } from "../abstract-class/abstract-tree-class";

export class TsSerializer extends ATreeSerializer {

    constructor(logger: ILogger) {
        super(logger);
    }

    serializeNode(node: AbstractSerializationClass, fsFileId: number): void {
        for(let _import of node.imports) {
            fs.writeSync(fsFileId, _import);
            fs.writeSync(fsFileId, '\n');
        }
        fs.writeSync(fsFileId, `export class ${node.className} {\n`);
        fs.writeSync(fsFileId, '\n');

        this.serializeProperties(node, fsFileId);
        this.serializeConstructor(node, fsFileId);
        this.serializeMethods(node, fsFileId);

        fs.writeSync(fsFileId, '}');
    }
    
    private serializeProperties(node: AbstractSerializationClass, fsFileId: number): void {
        for(let prop of node.properties) {
            fs.writeSync(fsFileId, `   ${prop.name}`);
            if(prop.type !== undefined) {
                fs.writeSync(fsFileId, `: ${prop.type}`);
            }
            if(prop.defaultValue !== undefined) {
                fs.writeSync(fsFileId, ` = ${prop.defaultValue}`);
            }
            fs.writeSync(fsFileId, ';\n');
        }
    }

    private serializeConstructor(node: AbstractSerializationClass, fsFileId: number): void {
        fs.writeSync(fsFileId, `   constructor(`);
        this.writeMethodParams(fsFileId, node.classConstructor.params);

        fs.writeSync(fsFileId, ')');
        fs.writeSync(fsFileId, node.classConstructor.code);
        fs.writeSync(fsFileId, '\n');
    }

    private serializeMethods(node: AbstractSerializationClass, fsFileId: number): void {
        for(let method of node.methods) {
            fs.writeSync(fsFileId, `   ${method.name}(`);
            this.writeMethodParams(fsFileId, method.params);
            fs.writeSync(fsFileId, ')');
            if(method.returnType !== undefined) {
                fs.writeSync(fsFileId, `: ${method.returnType}`);
            }
            fs.writeSync(fsFileId, '\n');
            fs.writeSync(fsFileId, method.code);
            fs.writeSync(fsFileId, '\n')
        }
    }

    private writeMethodParams(fsFileId: number, params: Array<FunctionParameter>): void {
        let first: boolean = true;
        for(let param of params) {
            if(first === true) {
                fs.writeSync(fsFileId, param.name);
                first = false;
            } else {
                fs.writeSync(fsFileId, `, ${param.name}`);
            }

            if(param.type !== undefined) {
                fs.writeSync(fsFileId, `: ${param.type}`);
            }
        }
    }

    serializeTree(treeClass: AbstractTreeClass, fsFileId: number): void {
        for(let _import of treeClass.imports) {
            fs.writeSync(fsFileId, _import);
            fs.writeSync(fsFileId, '\n');
        }
        fs.writeSync(fsFileId, `export class ${treeClass.className} {\n`);
        fs.writeSync(fsFileId, '\n');

        this.serializeProperties(treeClass, fsFileId);
        this.serializeConstructor(treeClass, fsFileId);
        this.serializeMethods(treeClass, fsFileId);

        fs.writeSync(fsFileId, '}');
    }
}