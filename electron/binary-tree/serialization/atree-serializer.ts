import { ILogger } from "../../logger/ilogger";
import { AbstractSerializationClass } from "./abstract-class/abstract-serialization-class";
import { BinaryTreeIpc } from "../../ipc/binary-tree-ipc";
import { AbstractNodeClass } from "./abstract-class/abstract-node-class";
import { AbstractTreeClass } from "./abstract-class/abstract-tree-class";

export abstract class ATreeSerializer {
    private logger: ILogger;

    constructor(logger: ILogger) {
        this.logger = logger;
    }

    abstract serializeNode(node: AbstractNodeClass, fsFileId: number): void;
    abstract serializeTree(treeClass: AbstractTreeClass, fsFileId: number): void;
}