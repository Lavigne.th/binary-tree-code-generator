import { TreeNodeIpc } from "../../../ipc/tree-node-ipc";
import { AbstractSerializationClass } from "./abstract-serialization-class";

export class AbstractNodeClass extends AbstractSerializationClass {

    public ipcNode: TreeNodeIpc;

    constructor(ipcNode: TreeNodeIpc) {
        super();
        this.ipcNode = ipcNode;
    }
}