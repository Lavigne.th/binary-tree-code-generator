export class FunctionParameter {
    public name: string;

    public type: string | undefined;

    constructor(name: string, type: string | undefined) {
        this.name = name;
        this.type = type;
    }
}