import { BinaryTreeIpc } from "../../../ipc/binary-tree-ipc";
import { AbstractSerializationClass } from "./abstract-serialization-class";

export class AbstractTreeClass extends AbstractSerializationClass {
    binaryTreeIpc: BinaryTreeIpc;

    constructor(binaryTreeIpc: BinaryTreeIpc) {
        super();
        this.binaryTreeIpc = binaryTreeIpc;
    }
}