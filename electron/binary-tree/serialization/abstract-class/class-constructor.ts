import { FunctionParameter } from "./function-parameter";

export class ClassConstructor {
    params: Array<FunctionParameter> = [];

    code: string;

    constructor(code: string) {
        this.code = code;
    }
}