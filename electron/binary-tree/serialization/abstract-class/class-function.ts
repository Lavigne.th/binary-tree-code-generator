import { FunctionParameter } from "./function-parameter";

export class ClassMethod {
    public name: string;

    public returnType: string | undefined;

    public params: Array<FunctionParameter>= [];

    public code: string;

    constructor(name: string, returnType: string | undefined, code: string) {
        this.name = name;
        this.returnType = returnType;
        this.code = code;
    }
}