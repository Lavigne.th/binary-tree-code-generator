import { TreeNodeIpc } from "../../../ipc/tree-node-ipc";
import { ClassConstructor } from "./class-constructor";
import { ClassMethod } from "./class-function";
import { ClassProperty } from "./class-property";

export abstract class AbstractSerializationClass {

    public className: string;

    public properties: Array<ClassProperty> = [];

    public methods: Array<ClassMethod>= [];

    public classConstructor: ClassConstructor;

    public imports: Array<string> = [];
    
}