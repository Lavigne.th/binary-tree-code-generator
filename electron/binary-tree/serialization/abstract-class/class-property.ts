export class ClassProperty {
    public name: string;

    public type: string | undefined;

    public defaultValue: string | undefined;

    constructor(name: string, type: string | undefined, defaultValue: string | undefined) {
        this.name = name;
        this.type = type;
        this.defaultValue = defaultValue;
    }
}