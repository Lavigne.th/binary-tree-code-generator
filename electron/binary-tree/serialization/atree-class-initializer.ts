import { BinaryTreeIpc } from "../../ipc/binary-tree-ipc";
import { TreeNodeIpc } from "../../ipc/tree-node-ipc";
import { ILogger } from "../../logger/ilogger";
import { AbstractNodeClass } from "./abstract-class/abstract-node-class";
import { AbstractSerializationClass } from "./abstract-class/abstract-serialization-class";
import { AbstractTreeClass } from "./abstract-class/abstract-tree-class";

export abstract class ATreeClassInitializer {
    private logger: ILogger;

    fileExtension: string;

    constructor(logger: ILogger, fileExtension: string) {
        this.logger = logger;
        this.fileExtension = fileExtension;
    }

    abstract initializeNode(node: AbstractNodeClass, binaryTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc | undefined, failureNode: TreeNodeIpc | undefined): void;
    abstract loadNode(node: AbstractNodeClass, currentTree: BinaryTreeIpc, previousTree: BinaryTreeIpc, isBranch: boolean, successNode: TreeNodeIpc | undefined, failureNode: TreeNodeIpc | undefined): void;
    
    abstract initializeTree(tree: AbstractTreeClass, rootNode: TreeNodeIpc): void;
    
    abstract getFileNameForNode(ipcNode: TreeNodeIpc): string;
    abstract getFileNameForTree(binaryTree: BinaryTreeIpc): string;
}