import * as fs from "fs"
import { BrowserWindow, ipcMain, IpcMainEvent } from 'electron'
import { BinaryTreeIpc } from '../ipc/binary-tree-ipc'
import { IPC_CHANNEL, SAVE_TREE_STATE } from '../ipc/ipc-channel'
import { BinaryTreeLoader } from "./loading/binary-tree-loader";
import { CONSTANTS } from "./constant";
import { TreeNodeIpc } from "../ipc/tree-node-ipc";
import { TsClassInitializer } from "./serialization/typescript/ts-class-initializer";
import { TsSerializer } from "./serialization/typescript/ts-serializer";
import { AbstractSerializationClass } from "./serialization/abstract-class/abstract-serialization-class";
import { ILogger } from "../logger/ilogger";
import { ATreeClassInitializer } from "./serialization/atree-class-initializer";
import { ATreeSerializer } from "./serialization/atree-serializer";
import { SUPPORTED_SERIALIZATION_LANGUAGE_ENUM } from "../ipc/supported-serialization-language-enum";
import { AbstractNodeClass } from "./serialization/abstract-class/abstract-node-class";
import { AbstractTreeClass } from "./serialization/abstract-class/abstract-tree-class";

export class BinaryTreeManager {

    private mainWindow: BrowserWindow;

    private logger: ILogger;

    loader: BinaryTreeLoader;

    constructor(mainWindow: BrowserWindow, logger: ILogger) {
        this.mainWindow = mainWindow;
        this.logger = logger;
        this.loader = new BinaryTreeLoader(mainWindow);

        ipcMain.on(IPC_CHANNEL.SAVE_BINARY_TREE, (e: IpcMainEvent, binaryTree: BinaryTreeIpc) => {
            let initializer: ATreeClassInitializer | undefined = undefined;
            let serializer: ATreeSerializer | undefined = undefined;
            if(binaryTree.serializationLanguage === SUPPORTED_SERIALIZATION_LANGUAGE_ENUM.TYPESCRIPT) {
                initializer = new TsClassInitializer(this.logger);
                serializer = new TsSerializer(this.logger);
            }
            if(initializer !== undefined && serializer !== undefined) {
                try {
                    this.mainWindow.webContents.send(IPC_CHANNEL.SAVE_TREE_STATE, SAVE_TREE_STATE.SAVING);
                    this.onSaveBinaryTree(binaryTree, initializer, serializer);
                } catch(ex: any) {
                    logger.logError(JSON.stringify(ex));
                } finally {
                    this.mainWindow.webContents.send(IPC_CHANNEL.SAVE_TREE_STATE, SAVE_TREE_STATE.ENDED);
                }
            } else {
                logger.logError(`Could not find initializer and serializer for language: ${binaryTree.serializationLanguage}`)
            }
        });
    }

    onSaveBinaryTree(binaryTree: BinaryTreeIpc, initializer: ATreeClassInitializer, serializer: ATreeSerializer): void {
        if (fs.existsSync(binaryTree.saveFolderPath)) {
            let confPath: string = `${binaryTree.saveFolderPath}/${CONSTANTS.TREE_CONFIGURATION_FILE}`;
            let existingTree: BinaryTreeIpc | undefined = undefined;
            if(fs.existsSync(confPath) === true) {
                const fileContents: string = fs.readFileSync(confPath).toString()
                existingTree = <BinaryTreeIpc> JSON.parse(fileContents);
            }
            this.createNodeDirectoryIfNeeded(binaryTree);

            this.saveBinaryTree(binaryTree, existingTree, initializer, serializer);
            this.deleteUnusedNodeFiles(binaryTree, existingTree , initializer);

            fs.writeFileSync(confPath, JSON.stringify(binaryTree, null, 2));
        } else {
            this.logger.logError("Path to save forlder doesn't exists: " + binaryTree.saveFolderPath);
        }
    }

    /**
     * Get the AbstractNodeClass et serialize them.
     * Get the AbstractTreeClass and serialize it
     * @param binaryTree 
     * @param previousTree 
     * @param initializer 
     * @param serializer 
     */
    saveBinaryTree(binaryTree: BinaryTreeIpc, previousTree: BinaryTreeIpc | undefined, initializer: ATreeClassInitializer, serializer: ATreeSerializer) {
        let nodeClasses: Array<AbstractNodeClass> = this.getNodeAbstractClasses(binaryTree, previousTree, initializer);
        this.serializeNodeClasses(binaryTree, nodeClasses, initializer, serializer);

        let treeClass: AbstractTreeClass = new AbstractTreeClass(binaryTree);
        let rootNode: TreeNodeIpc = binaryTree.nodes.find(n => n.id === binaryTree.rootNodeId);
        initializer.initializeTree(treeClass, rootNode);
        this.serializeTreeClass(binaryTree, treeClass, initializer, serializer);
    }

    private serializeTreeClass(binaryTree: BinaryTreeIpc, treeClass: AbstractTreeClass, initializer: ATreeClassInitializer, serializer: ATreeSerializer): void {
        let treeClassPath: string = `${binaryTree.saveFolderPath}/${initializer.getFileNameForTree(binaryTree)}.${initializer.fileExtension}`;
        binaryTree.treeClassFile = `${initializer.getFileNameForTree(binaryTree)}.${initializer.fileExtension}`;
        let fsFileId: number = fs.openSync(treeClassPath, 'w');
        serializer.serializeTree(treeClass, fsFileId);
        fs.closeSync(fsFileId);
    }

    private getNodeAbstractClasses(binaryTree: BinaryTreeIpc, previousTree: BinaryTreeIpc | undefined, initializer: ATreeClassInitializer): Array<AbstractNodeClass> {
        let nodeClasses: Array<AbstractNodeClass> = [];
        let processedNodeIds: Array<number> = [];
        for(let node of binaryTree.nodes) {
            if(node.hasBeenCloned === true && processedNodeIds.includes(node.id)) {
                this.logger.logInfo(node.id.toString());
                continue;
            }
            processedNodeIds.push(node.id);
            let currentNodeClass: AbstractNodeClass = new AbstractNodeClass(node);
            let isBranch: boolean = TreeNodeIpc.isBranch(node);
            let successNode: TreeNodeIpc | undefined = undefined;
            let failureNode: TreeNodeIpc | undefined = undefined;
            //Get success and failure node if the node is a branch
            if(isBranch === true) {
                successNode = binaryTree.nodes.find(n => n.id === node.successNode.id);
                failureNode = binaryTree.nodes.find(n => n.id === node.failureNode.id);
                if(successNode === undefined || failureNode === undefined) {
                    isBranch = false;
                }
            }
            initializer.initializeNode(currentNodeClass, binaryTree, isBranch, successNode, failureNode);
            if(previousTree !== undefined) {
                initializer.loadNode(currentNodeClass, binaryTree, previousTree, isBranch, successNode, failureNode);
            }
            nodeClasses.push(currentNodeClass);
        }
        return nodeClasses;
    }

    private serializeNodeClasses(binaryTree: BinaryTreeIpc, nodeClasses: Array<AbstractNodeClass>, initializer: ATreeClassInitializer, serializer: ATreeSerializer): void {
        let nodesPath: string = `${binaryTree.saveFolderPath}/${CONSTANTS.TREE_NODES_SAVE_FOLDER}`;
        for(let nodeClass of nodeClasses) {
            let nodeFilePath: string = `${nodesPath}/${initializer.getFileNameForNode(nodeClass.ipcNode)}.${initializer.fileExtension}`;
            nodeClass.ipcNode.saveFile = `${CONSTANTS.TREE_NODES_SAVE_FOLDER}/${initializer.getFileNameForNode(nodeClass.ipcNode)}.${initializer.fileExtension}`;
            
            let fsFileId: number = fs.openSync(nodeFilePath, 'w');
            serializer.serializeNode(nodeClass, fsFileId);
            fs.closeSync(fsFileId);
        }
    }

    createNodeDirectoryIfNeeded(binaryTree: BinaryTreeIpc): void {
        let hasNodeDirectory: boolean = false;
        for(let name of fs.readdirSync(binaryTree.saveFolderPath)) {
            if(fs.statSync(`${binaryTree.saveFolderPath}/${name}`).isDirectory() === true && name === CONSTANTS.TREE_NODES_SAVE_FOLDER) {
                hasNodeDirectory = true;
            }
        }
        if(hasNodeDirectory === false) {
            fs.mkdirSync(`${binaryTree.saveFolderPath}/${CONSTANTS.TREE_NODES_SAVE_FOLDER}`); 
        }
    }

    /**
     * Delete node that had they file name changed or were removed from the new tree
     * @param currentTree 
     * @param previousTree 
     * @param inializer 
     */
    deleteUnusedNodeFiles(currentTree: BinaryTreeIpc, previousTree: BinaryTreeIpc | undefined, inializer: ATreeClassInitializer): void {
        if(previousTree !== undefined) {
            for(let previousNode of previousTree.nodes) {
                let matchInCurrentTree: TreeNodeIpc | undefined = currentTree.nodes.find(n => n.saveFile === previousNode.saveFile);
                if(matchInCurrentTree === undefined) {
                    let filePath: string = `${previousTree.saveFolderPath}/${previousNode.saveFile}`;
                    if(fs.existsSync(filePath) === true) {
                        fs.unlinkSync(filePath);
                    }
                }
            }
        }
    }
}