export enum IPC_CHANNEL {
    CHOOSE_FOLDER = 'choose_folder',
    FOLDER_CHOSEN = 'choose_chosen',
    RELOAD_PAGE = 'reload_page',
    SAVE_BINARY_TREE = 'save_tree',
    LOAD_BINARY_TREE = 'load_tree',
    LOAD_TREE_FAILED = 'load_tree_failed',
    LOAD_TREE_RESULT = 'load_tree_res',
    LOG = 'log',
    SAVE_TREE_STATE = 'save_tree_state'
}

export enum SAVE_TREE_STATE {
    SAVING = 'started',
    ENDED = 'ended'
}