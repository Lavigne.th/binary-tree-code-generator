export class TreeNodeIpc {
    id: number;
    instanceId: number;
    name: string;
    saveFile?: string;

    y: number;
    x: number;

    parentNode: TreeNodeRelation | undefined;
    successNode: TreeNodeRelation | undefined;
    failureNode: TreeNodeRelation | undefined;
    hasBeenCloned: boolean;

    constructor(id: number, instanceId: number, name: string, x: number, y: number, parentNode: TreeNodeRelation | undefined, successNode: TreeNodeRelation | undefined, failureNode: TreeNodeRelation | undefined, hasBeenCloned: boolean) {
        this.id = id;
        this.instanceId = instanceId;
        this.name = name;
        this.x = x;
        this.y = y;
        this.parentNode = parentNode;
        this.successNode = successNode;
        this.failureNode = failureNode;
        this.hasBeenCloned = hasBeenCloned;
    }

    static isBranch(ipcNode: TreeNodeIpc) {
        return ipcNode.successNode !== undefined && ipcNode.failureNode !== undefined;
    }
}

export class TreeNodeRelation {
    id: number;
    instanceId: number;

    constructor(id: number, instanceId: number)  {
        this.id = id;
        this.instanceId = instanceId;
    }
}