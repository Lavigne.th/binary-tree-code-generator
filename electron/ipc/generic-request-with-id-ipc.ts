export class GenericRequestwithIdIpc {
    private static CURRENT_ID: number = 0;

    id: number;

    constructor() {
        this.id = ++GenericRequestwithIdIpc.CURRENT_ID;
    }
}