export class FolderChosenIpc {
    id: number;
    folder: string;
    canceled: boolean;

    constructor(id: number, folder: string, canceled: boolean) {
        this.id = id;
        this.folder = folder;
        this.canceled = canceled;
    }
}