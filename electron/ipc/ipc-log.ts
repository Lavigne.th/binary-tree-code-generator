import { IPC_LOG_LEVEL } from "../logger/ipc-logger";

export class IpcLog {
    message: string;

    level: IPC_LOG_LEVEL;

    constructor(message: string, level: IPC_LOG_LEVEL) {
        this.message = message;
        this.level = level;
    }
}