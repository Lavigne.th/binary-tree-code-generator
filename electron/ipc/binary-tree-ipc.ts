import { SUPPORTED_SERIALIZATION_LANGUAGE_ENUM } from "./supported-serialization-language-enum";
import { TreeNodeIpc } from "./tree-node-ipc";

export class BinaryTreeIpc {
    saveFolderPath: string;
    treeName: string;
    apiRelativePath: string;
    apiClassName: string;
    nodes: Array<TreeNodeIpc>;
    rootNodeId: number;
    currentZoom: number;
    serializationLanguage: SUPPORTED_SERIALIZATION_LANGUAGE_ENUM;
    treeClassFile?: string;

    constructor(saveFolderPath: string, treeName: string, rootNodeId: number, apiRelativePath: string, apiClassName: string, nodes: Array<TreeNodeIpc>, currentZoom: number, serializationLanguage: SUPPORTED_SERIALIZATION_LANGUAGE_ENUM) {
        this.saveFolderPath = saveFolderPath;
        this.treeName = treeName;
        this.rootNodeId = rootNodeId;
        this.apiRelativePath = apiRelativePath;
        this.apiClassName = apiClassName;
        this.nodes = nodes;
        this.currentZoom = currentZoom;
        this.serializationLanguage = serializationLanguage;
    }
}