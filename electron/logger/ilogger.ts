export interface ILogger {
    logInfo(message: string): void;
    logDanger(message: string): void;
    logError(meessage: string): void;
}