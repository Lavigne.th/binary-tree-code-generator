import { BrowserWindow } from "electron";
import { IPC_CHANNEL } from "../ipc/ipc-channel";
import { IpcLog } from "../ipc/ipc-log";
import { ILogger } from "./ilogger";

export enum IPC_LOG_LEVEL {
    info = 'info',
    danger = 'danger',
    error = 'error'
}

export class IpcLogger implements ILogger {

    mainWindow: BrowserWindow;

    constructor(mainWindow: BrowserWindow) {
        this.mainWindow = mainWindow;
    }

    logInfo(message: string): void{
        this.mainWindow.webContents.send(IPC_CHANNEL.LOG, new IpcLog(message, IPC_LOG_LEVEL.info));
        console.log(message);
    }

    logDanger(message: string): void {
        this.mainWindow.webContents.send(IPC_CHANNEL.LOG, new IpcLog(message, IPC_LOG_LEVEL.danger));
        console.log(message);
    }

    logError(message: string): void {
        this.mainWindow.webContents.send(IPC_CHANNEL.LOG, new IpcLog(message, IPC_LOG_LEVEL.error));
        console.log(message);
    }

}