import { app, BrowserWindow, ipcMain, ipcRenderer, dialog, IpcMainEvent } from 'electron'
import * as path from 'path'
import { BinaryTreeManager } from './binary-tree/binary-tree-manager'
import { FolderChosenIpc } from './ipc/folder-chosen-ipc'
import { GenericRequestwithIdIpc } from './ipc/generic-request-with-id-ipc'
import { IPC_CHANNEL } from './ipc/ipc-channel'
import { ILogger } from './logger/ilogger'
import { IpcLogger } from './logger/ipc-logger'

const createWindow = () => {
  const mainWindow = new BrowserWindow({
    title: "Binary tree generator",
    width: 1500,
    height: 600,
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  const logger: ILogger = new IpcLogger(mainWindow);

  mainWindow.loadFile('./dist/binary-tree-generator/index.html');

  mainWindow.webContents.openDevTools()
  mainWindow.webContents.once('dom-ready', () => {
    var binaryTreeManager: BinaryTreeManager = new BinaryTreeManager(mainWindow, logger);
  });

  mainWindow.on('close', () => {
  });

  ipcMain.on(IPC_CHANNEL.RELOAD_PAGE, () => {
    mainWindow.loadFile('./dist/binary-tree-generator/index.html');
  });
}



ipcMain.on(IPC_CHANNEL.CHOOSE_FOLDER, (e: IpcMainEvent, message: GenericRequestwithIdIpc) => {
  dialog.showOpenDialog({
    properties: ['openDirectory']
  }).then((value: Electron.OpenDialogReturnValue) => {
    let folderName: string = value.filePaths.length > 0 ? value.filePaths[0] : '';
    e.sender.send(IPC_CHANNEL.FOLDER_CHOSEN, new FolderChosenIpc(message.id, folderName, value.canceled));
  });
});

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
});

